import Vue from "vue";
import Router from "vue-router";
import Accueil from "./components/Accueil.vue"
import Membres from "./components/Membres.vue"
import PresentationMembres from "./components/PresentationMembres.vue"
import PresentationProjet from "./components/PresentationProjet.vue"
import Contacts from "./components/Contacts.vue"

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      redirect: "/accueil" 
    },
    {
      path: "/accueil",
      name: "Accueil",
      component: Accueil
    },
    {
      path: "/membres",
      name: "Membres",
      component: Membres
    },
    {
      path: "/presentation-membres",
      name: "PresentationMembres",
      component: PresentationMembres
    },
    {
      path: "/presentation-projet",
      name: "PresentationProjet",
      component: PresentationProjet
    },
    {
      path: "/contacts",
      name: "Contacts",
      component: Contacts
    }
  ]
});
